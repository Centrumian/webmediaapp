﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Wangkanai.Detection;

namespace WebMediaApplication.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class ApiController : Controller
    {
        private readonly IBrowser _browser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly EventContext _eventContext;
        private readonly ILogger _logger;

        public ApiController(ILogger<ApiController> logger,
                             IBrowserResolver browserResolver, 
                             IHttpContextAccessor httpContextAccessor, 
                             EventContext eventContext)
        {
            _logger = logger;
            _browser = browserResolver.Browser;
            _httpContextAccessor = httpContextAccessor;
            _eventContext = eventContext;
        }

        [Route("save")]
        public void Save(string type)
        {
            try
            {
                //   var userGuid = HttpContext.User.Identity.IsAuthenticated ?
                //_httpContextAccessor.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value :
                //Guid.Empty.ToString();

                var userGuid = Guid.NewGuid();
                EventInfo eInfo = new EventInfo()
                {
                    BrowserName = _browser.Type.ToString(),
                    BrowserVersion = _browser.Version.ToString(),
                    Date = DateTime.UtcNow,
                    Type = type,
                    UserGuid = userGuid
                };

                _eventContext.Events.Add(eInfo);
                _eventContext.SaveChanges();
            }
            catch (Exception exc)
            {
                _logger.LogError($"Exception occured in saving event to database. Error : {exc.Message}");
            }
        }

        [Route("getpostersrc")]
        public string GetPosterSrc()
        {
            return @"https://pic.showjet.ru/pics/1ca8ae91-a317-431a-9f0b-10a93b57eca9/7x10/450.jpg";
        }
    }
}
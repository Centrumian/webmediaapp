using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace WebMediaApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)

                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .UseSerilog((hostingContext, loggerConfiguration) => loggerConfiguration
                    .ReadFrom.Configuration(hostingContext.Configuration)
                    .Enrich.FromLogContext()
                    .WriteTo.Debug()
                .WriteTo.File(
                    path: $"Log/webapi--{DateTime.UtcNow.Date.Day}.{DateTime.UtcNow.Date.Month}.{DateTime.UtcNow.Date.Year} {DateTime.UtcNow.Hour}hr.log",
                    outputTemplate: "[{Timestamp:dd:MM:yyyy HH:mm} {Level:u3}] {Message:lj} {NewLine}"));
    }
}

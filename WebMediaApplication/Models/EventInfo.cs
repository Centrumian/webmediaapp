﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMediaApplication
{
    public class EventInfo
    {
        public int Id { get; set; }

        public Guid UserGuid { get; set; }

        public string BrowserName { get; set; }

        public string BrowserVersion { get; set; }

        public string Type { get; set; }

        public DateTime Date { get; set; }
    }
}

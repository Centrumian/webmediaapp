﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TestClient
{
    class Program
    {
        static HttpClient webclient;
        static void Main(string[] args)
        {
            webclient = new HttpClient();
            webclient.BaseAddress = new Uri("https://localhost:5001/");

            Timer dispatcher = new Timer();
            dispatcher.Elapsed += Dispatcher_Tick;
            dispatcher.Interval = 1000;
            dispatcher.Start();

            Console.ReadLine();
        }

        private static void Dispatcher_Tick(object sender, EventArgs e)
        {
           for (int i=0; i < 3000; i++)
            {
                webclient.GetAsync(@"/api/save?type=test").Wait();
            }
        }
    }
}
